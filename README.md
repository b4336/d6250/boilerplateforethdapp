This directory contains boilerplate folders and files with which a decentralized application (dapp) can be built. To build a dapp, simply git clone this directory with a new name for the dapp project. Make sure the packages and their version specified in the package.json file match your project's needs.

Concretely, this directory was made with the following steps:

1. Initiated on GitLab as a blank project, containing only the default README.md file
2. Made a React project by running "npx create-react-app"
3. Made a directory named "components" inside the src directory
4. Moved App.js and App.css to components directory and deleted unnecessary files (e.g. logos)
5. Made up the package.json file with necessary dependencies and npm installed them
6. Made a Truffle project by running "truffle init"
7. Moved the contracts directory to the src directory
8. Configured the truffle-config.js to match project requirement
9. Touched a .babelrc file to include JavaScript ES6